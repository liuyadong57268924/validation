<?php

namespace Itwmw\Validation\Tests\Test;

use Itwmw\Validation\Tests\Bin\BaseTest;

class ValidatesAttributesTest extends BaseTest
{
    public function testValidateAccepted()
    {
        $this->checkRule(['a' => 1], ['a' => 'accepted'], true);
        $this->checkRule(['a' => true], ['a' => 'accepted'], true);
        $this->checkRule(['a' => 0], ['a' => 'accepted'], false, '您必须接受 a。');
    }

    public function testValidateSize()
    {
        $this->checkRule('name', 'string|size:4', true);
        $this->checkRule(100, 'numeric|size:100', true);
        $this->checkRule(['s' => [1, 2, 3]], 'array|size:3', true);
        $this->checkRule(['l' => 5, 's' => 'world'], ['s' => 'size:l'], true);
        $this->checkRule(['l' => 2, 's' => 2], ['s' => 'numeric|size:l'], true);
        $this->checkRule(['l' => 2, 's' => [1, 2]], ['s' => 'array|size:l'], true);

        $this->checkRule('name', 'string|size:3', false, 's 必须是 3 个字符。');
        $this->checkRule(100, 'numeric|size:500', false, 's 大小必须为 500。');
        $this->checkRule(['s' => [1, 2, 3]], 'array|size:4', false, 's 必须为 4 个单元。');
        $this->checkRule(['l' => 6, 's' => 'world'], ['s' => 'size:l'], false, 's 必须是 6 个字符。');
        $this->checkRule(['l' => 2, 's' => 3], ['s' => 'numeric|size:l'], false, 's 大小必须为 2。');
        $this->checkRule(['l' => 3, 's' => [1, 2]], ['s' => 'array|size:l'], false, 's 必须为 3 个单元。');
    }
}
