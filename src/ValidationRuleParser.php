<?php

namespace Itwmw\Validation;

use Closure;
use Illuminate\Support\Arr;
use Itwmw\Validation\Support\Str;
use Itwmw\Validation\Support\Interfaces\Rule as RuleContract;

class ValidationRuleParser
{
    /**
     * The data being validated.
     *
     * @var array
     */
    public $data;

    /**
     * The implicit attributes.
     *
     * @var array
     */
    public $implicitAttributes = [];

    /**
     * 创建一个新的验证规则解析器。
     *
     * @param  array  $data
     * @return void
     */
    public function __construct(array $data)
    {
        $this->data = $data;
    }

    /**
     * 将人性化的规则解析为验证器的完整规则阵列。
     *
     * @param  array  $rules
     * @return \stdClass
     */
    public function explode(array $rules)
    {
        $this->implicitAttributes = [];

        $rules = $this->explodeRules($rules);

        return (object) [
            'rules'              => $rules,
            'implicitAttributes' => $this->implicitAttributes,
        ];
    }

    /**
     * 将规则分解成一个显式规则阵列。
     *
     * @param  array  $rules
     * @return array
     */
    protected function explodeRules(array $rules): array
    {
        foreach ($rules as $key => $rule) {
            if (Str::contains($key, '*')) {
                $rules = $this->explodeWildcardRules($rules, $key, [$rule]);

                unset($rules[$key]);
            } else {
                $rules[$key] = $this->explodeExplicitRule($rule);
            }
        }

        return $rules;
    }

    /**
     * 如果有必要的话，将显式规则分解成一个数组。
     *
     * @param  mixed  $rule
     * @return array
     */
    protected function explodeExplicitRule($rule): array
    {
        if (is_string($rule)) {
            return explode('|', $rule);
        } elseif (is_object($rule)) {
            return [$this->prepareRule($rule)];
        }

        return array_map([$this, 'prepareRule'], $rule);
    }

    /**
     * 为验证器准备好给定的规则。
     *
     * @param  mixed  $rule
     * @return mixed
     */
    protected function prepareRule($rule)
    {
        if ($rule instanceof Closure) {
            $rule = new ClosureValidationRule($rule);
        }

        if (! is_object($rule) || $rule instanceof RuleContract) {
            return $rule;
        }

        return (string) $rule;
    }

    /**
     * 定义一套适用于数组属性中每个元素的规则。
     *
     * @param  array  $results
     * @param  string  $attribute
     * @param  string|array  $rules
     * @return array
     */
    protected function explodeWildcardRules(array $results, string $attribute, $rules): array
    {
        $pattern = str_replace('\*', '[^\.]*', preg_quote($attribute));

        $data = ValidationData::initializeAndGatherData($attribute, $this->data);

        foreach ($data as $key => $value) {
            if (Str::startsWith($key, $attribute) || (bool) preg_match('/^' . $pattern . '\z/', $key)) {
                foreach ((array) $rules as $rule) {
                    $this->implicitAttributes[$attribute][] = $key;

                    $results = $this->mergeRules($results, $key, $rule);
                }
            }
        }

        return $results;
    }

    /**
     * 将额外的规则合并到一个（多个）给定的属性中。
     *
     * @param  array  $results
     * @param  string|array  $attribute
     * @param  string|array  $rules
     * @return array
     */
    public function mergeRules(array $results, $attribute, $rules = []): array
    {
        if (is_array($attribute)) {
            foreach ((array) $attribute as $innerAttribute => $innerRules) {
                $results = $this->mergeRulesForAttribute($results, $innerAttribute, $innerRules);
            }

            return $results;
        }

        return $this->mergeRulesForAttribute(
            $results,
            $attribute,
            $rules
        );
    }

    /**
     * 将额外的规则合并到一个给定的属性中。
     *
     * @param  array  $results
     * @param  string  $attribute
     * @param  string|array  $rules
     * @return array
     */
    protected function mergeRulesForAttribute(array $results, string $attribute, $rules): array
    {
        $merge = head($this->explodeRules([$rules]));

        $results[$attribute] = array_merge(
            isset($results[$attribute]) ? $this->explodeExplicitRule($results[$attribute]) : [],
            $merge
        );

        return $results;
    }

    /**
     * 从一个规则中提取规则名称和参数。
     *
     * @param  array|string|RuleContract|Closure  $rule
     * @return array
     */
    public static function parse($rule): array
    {
        if ($rule instanceof RuleContract) {
            return [$rule, []];
        }

        if (is_array($rule)) {
            $rule = static::parseArrayRule($rule);
        } else {
            $rule = static::parseStringRule($rule);
        }

        $rule[0] = static::normalizeRule($rule[0]);

        return $rule;
    }

    /**
     * 解析一个基于数组的规则。
     *
     * @param  array  $rules
     * @return array
     */
    protected static function parseArrayRule(array $rules): array
    {
        return [Str::studly(trim(Arr::get($rules, 0, ''))), array_slice($rules, 1)];
    }

    /**
     * 解析一个基于字符串的规则。
     *
     * @param  string  $rules
     * @return array
     */
    protected static function parseStringRule($rules): array
    {
        $parameters = [];
        
        // 指定验证规则和参数的格式遵循简单的{规则}：{参数}的格式约定。
        // 例如，规则 "Max:3 "规定该值只能是三个字母。
        if (false !== strpos($rules, ':')) {
            [$rules, $parameter] = explode(':', $rules, 2);

            $parameters = static::parseParameters($rules, $parameter);
        }

        return [Str::studly(trim($rules)), $parameters];
    }

    /**
     * 解析一个参数列表。
     *
     * @param  string  $rule
     * @param  string  $parameter
     * @return array
     */
    protected static function parseParameters(string $rule, string $parameter): array
    {
        $rule = strtolower($rule);

        if (in_array($rule, ['regex', 'not_regex', 'notregex'], true)) {
            return [$parameter];
        }

        return str_getcsv($parameter);
    }

    /**
     * 将一个规则规范化，以便我们可以接受短类型。
     *
     * @param  string  $rule
     * @return string
     */
    protected static function normalizeRule(string $rule): string
    {
        switch ($rule) {
            case 'Int':
                return 'Integer';
            case 'Bool':
                return 'Boolean';
            default:
                return $rule;
        }
    }
}
