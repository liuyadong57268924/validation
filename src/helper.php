<?php

use Itwmw\Validation\Support\Collection\Collection;

if (!function_exists('validate_collect')) {
    /**
     * 从给定值创建一个Collection
     *
     * @param null $value
     * @return Collection
     */
    function validate_collect($value = null): Collection
    {
        return new Collection($value);
    }
}

if (! function_exists('head')) {
    /**
     * 获取数组的第一个元素。对方法链很有用。
     *
     * @param array $array
     * @return mixed
     */
    function head(array $array)
    {
        return reset($array);
    }
}

if (! function_exists('last')) {
    /**
     * 从数组中获取最后一个元素
     *
     * @param  array  $array
     * @return mixed
     */
    function last($array)
    {
        return end($array);
    }
}

if (! function_exists('class_basename')) {
    /**
     * 获取给定对象/类的basename
     *
     * @param  string|object  $class
     * @return string
     */
    function class_basename($class): string
    {
        $class = is_object($class) ? get_class($class) : $class;

        return basename(str_replace('\\', '/', $class));
    }
}
