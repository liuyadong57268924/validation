<?php

namespace Itwmw\Validation;

use Closure;
use Illuminate\Support\Arr;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Itwmw\Validation\Support\Concerns\FormatsMessages;
use Itwmw\Validation\Support\Concerns\ValidatesAttributes;
use Itwmw\Validation\Support\Interfaces\ImplicitRule;
use Itwmw\Validation\Support\Interfaces\PresenceVerifierInterface;
use Itwmw\Validation\Support\Interfaces\Rule;
use Itwmw\Validation\Support\Str;
use Itwmw\Validation\Support\Translation\Translator;
use Itwmw\Validation\Support\ValidationException;

class Validator
{
    use FormatsMessages;
    use ValidatesAttributes;

    /**
     * 当前规则键中点的占位符
     *
     * @var string
     */
    protected $dotPlaceholder;

    /**
     * 初始规则
     *
     * @var array
     */
    protected $initialRules;

    /**
     * 自定义错误消息
     *
     * @var array
     */
    public $customMessages = [];

    /**
     * 要应用于数据的规则。
     *
     * @var array
     */
    protected $rules;

    /**
     * 目前正在验证的规则。
     *
     * @var string
     */
    protected $currentRule;

    /**
     * 带有星号的通配符属性阵列。
     *
     * @var array
     */
    protected $implicitAttributes = [];
    
    /**
     * 自定义属性名称
     *
     * @var array
     */
    public $customAttributes = [];

    /**
     * 备用错误信息的数组。
     *
     * @var array
     */
    public $fallbackMessages = [];

    /**
     * 翻译器的实例
     *
     * @var Translator
     */
    protected $translator;

    /**
     * 所有的自定义替换器扩展
     *
     * @var array
     */
    public $replacers = [];

    /**
     * 所有的自定义验证器扩展。
     *
     * @var array
     */
    public $extensions = [];

    /**
     * 存在验证器的实例
     *
     * @var PresenceVerifierInterface
     */
    protected $presenceVerifier;

    /**
     * 应该用于格式化属性的回调。
     *
     * @var callable|null
     */
    protected $implicitAttributesFormatter;

    /**
     * 可适用于文件的验证规则。
     *
     * @var array
     */
    protected $fileRules = [
        'Between',
        'Dimensions',
        'File',
        'Image',
        'Max',
        'Mimes',
        'Mimetypes',
        'Min',
        'Size',
    ];

    /**
     * 意味着该字段为必填字段的验证规则。
     *
     * @var array
     */
    protected $implicitRules = [
        'Accepted',
        'AcceptedIf',
        'Declined',
        'DeclinedIf',
        'Filled',
        'Present',
        'Required',
        'RequiredIf',
        'RequiredUnless',
        'RequiredWith',
        'RequiredWithAll',
        'RequiredWithout',
        'RequiredWithoutAll',
    ];

    /**
     * 依靠其他字段作为参数的验证规则。
     *
     * @var array
     */
    protected $dependentRules = [
        'After',
        'AfterOrEqual',
        'Before',
        'BeforeOrEqual',
        'Confirmed',
        'Different',
        'ExcludeIf',
        'ExcludeUnless',
        'ExcludeWithout',
        'Gt',
        'Gte',
        'Lt',
        'Lte',
        'AcceptedIf',
        'RequiredIf',
        'RequiredUnless',
        'RequiredWith',
        'RequiredWithAll',
        'RequiredWithout',
        'RequiredWithoutAll',
        'Prohibited',
        'ProhibitedIf',
        'ProhibitedUnless',
        'Prohibits',
        'Same',
        'Unique',
    ];

    /**
     * 可以排除属性的验证规则。
     *
     * @var array
     */
    protected $excludeRules = ['Exclude', 'ExcludeIf', 'ExcludeUnless', 'ExcludeWithout'];

    /**
     * 尺寸相关的验证规则。
     *
     * @var array
     */
    protected $sizeRules = ['Size', 'Between', 'Min', 'Max', 'Gt', 'Lt', 'Gte', 'Lte'];

    /**
     * 数字相关的验证规则。
     *
     * @var array
     */
    protected $numericRules = ['Numeric', 'Integer'];

    /**
     * 记录验证是否执行过
     *
     * @var bool
     */
    protected $passed;
    
    /**
     * 正在验证的数据。
     *
     * @var array
     */
    protected $data;

    /**
     * 验证失败异常
     *
     * @var ?ValidationException
     */
    protected $errorException;

    public function __construct(
        Translator $translator,
        array $data,
        array $rules,
        array $messages = [],
        array $customAttributes = []
    ) {
        $this->dotPlaceholder   = Str::random();
        $this->initialRules     = $rules;
        $this->customMessages   = $messages;
        $this->data             = $this->parseData($data);
        $this->customAttributes = $customAttributes;
        $this->translator       = $translator;

        $this->setRules($rules);
    }

    /**
     * 解析数据阵列，转换点和星号。
     *
     * @param  array  $data
     * @return array
     */
    public function parseData(array $data): array
    {
        $newData = [];

        foreach ($data as $key => $value) {
            if (is_array($value)) {
                $value = $this->parseData($value);
            }

            $key = str_replace(
                ['.', '*'],
                [$this->dotPlaceholder, '__asterisk__'],
                $key
            );

            $newData[$key] = $value;
        }

        return $newData;
    }

    /**
     * 设置验证规则。
     *
     * @param  array  $rules
     * @return $this
     */
    public function setRules(array $rules): Validator
    {
        $rules = validate_collect($rules)->mapWithKeys(function ($value, $key) {
            return [str_replace('\.', $this->dotPlaceholder, $key) => $value];
        })->toArray();

        $this->initialRules = $rules;

        $this->rules = [];

        $this->addRules($rules);

        return $this;
    }

    /**
     * 解析给定的规则，并将其合并到当前规则中。
     *
     * @param  array  $rules
     * @return void
     */
    public function addRules($rules)
    {
        // 这个解析器的主要目的是将任何 "*"规则扩展为给定数据所需的所有明确规则。
        // 例如，规则names.*将被扩展为该数据的names.0、names.1等。
        $response = (new ValidationRuleParser($this->data))
            ->explode($rules);

        $this->rules = array_merge_recursive($this->rules, $response->rules);

        $this->implicitAttributes = array_merge($this->implicitAttributes, $response->implicitAttributes);
    }

    /**
     * 确定数据是否通过验证规则。
     *
     * @param bool $throws
     * @return bool
     *
     * @throws ValidationException
     */
    public function passes(bool $throws = false): bool
    {
        $this->passed = false;
        try {
            foreach ($this->rules as $attribute => $rules) {
                foreach ($rules as $rule) {
                    $this->validateAttribute($attribute, $rule);
                }
            }
        } catch (ValidationException $e) {
            $this->passed = true;
            if ($throws) {
                throw $e;
            } else {
                $this->errorException = $e;
                return false;
            }
        }

        $this->passed = true;
        return true;
    }

    public function error(): ?ValidationException
    {
        return $this->errorException;
    }

    /**
     * 验证并获取结果
     *
     * @return array
     * @throws ValidationException
     */
    public function validate(): array
    {
        if (!$this->passed) {
            $this->passes(true);
        }
        
        $results = [];

        $missingValue = Str::random(10);

        foreach (array_keys($this->getRules()) as $key) {
            $value = data_get($this->getData(), $key, $missingValue);

            if ($value !== $missingValue) {
                Arr::set($results, $key, $value);
            }
        }

        return $this->replacePlaceholders($results);
    }

    /**
     * 根据规则验证一个给定的属性。
     *
     * @param string $attribute
     * @param string|Closure $rule
     * @return void
     *
     * @throws ValidationException
     */
    protected function validateAttribute(string $attribute, $rule)
    {
        $this->currentRule = $rule;

        [$rule, $parameters] = ValidationRuleParser::parse($rule);

        if ('' == $rule) {
            return;
        }

        // 首先，我们将在字段嵌套在数组中的情况下，获取给定属性的正确键。
        // 然后我们确定给定的规则是否接受其他字段名作为参数。
        // 如果接受，我们将用正确的键来替换参数中的星号。
        if (($keys = $this->getExplicitKeys($attribute))
            && $this->dependsOnOtherFields($rule)) {
            $parameters = $this->replaceAsterisksInParameters($parameters, $keys);
        }

        $value = $this->getValue($attribute);
        // 如果属性是文件，我们将验证文件上传是否真的成功，
        // 如果不成功，我们将为该属性添加一个失败。
        // 根据PHP的设置，如果文件太大，可能无法成功上传，所以在这种情况下我们将放弃。
        if ($value instanceof UploadedFile && !$value->isValid()
            && $this->hasRule($attribute, array_merge($this->fileRules, $this->implicitRules))) {
            $this->throwFailure($attribute, 'uploaded', []);
        }

        // 如果我们已经走到了这一步，我们将确保该属性是可验证的，
        // 如果是，我们将调用该属性的验证方法。
        $validatable = $this->isValidatable($rule, $attribute, $value);

        if ($rule instanceof Rule) {
            if ($validatable) {
                $this->validateUsingCustomRule($attribute, $value, $rule);
            }
            return;
        }

        $method = "validate{$rule}";

        if ($validatable && ! $this->$method($attribute, $value, $parameters, $this)) {
            $this->throwFailure($attribute, $rule, $parameters);
        }
    }

    /**
     * 使用自定义规则对象验证一个属性。
     *
     * @param string $attribute
     * @param mixed $value
     * @param Rule $rule
     * @return void
     *
     * @throws ValidationException
     */
    protected function validateUsingCustomRule(string $attribute, $value, Rule $rule)
    {
        $attribute = $this->replacePlaceholderInString($attribute);

        $value = is_array($value) ? $this->replacePlaceholders($value) : $value;
        
        if (!$rule->passes($attribute, $value)) {
            $message = $rule->message();

            throw (new ValidationException($this->makeReplacements(
                $message,
                $attribute,
                get_class($rule),
                []
            )))->setAttribute($attribute);
        }
    }
    /**
     * 替换数据键中使用的占位符.
     *
     * @param  array  $data
     * @return array
     */
    protected function replacePlaceholders(array $data): array
    {
        $originalData = [];

        foreach ($data as $key => $value) {
            $originalData[$this->replacePlaceholderInString($key)] = is_array($value)
                ? $this->replacePlaceholders($value)
                : $value;
        }

        return $originalData;
    }

    /**
     * 替换给定字符串中的占位符。
     *
     * @param  string  $value
     * @return string
     */
    protected function replacePlaceholderInString(string $value): string
    {
        return str_replace(
            [$this->dotPlaceholder, '__asterisk__'],
            ['.', '*'],
            $value
        );
    }

    /**
     * 确定属性是否可验证。
     *
     * @param  object|string  $rule
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    protected function isValidatable($rule, string $attribute, $value): bool
    {
        if (in_array($rule, $this->excludeRules)) {
            return true;
        }

        return $this->presentOrRuleIsImplicit($rule, $attribute, $value)
            && $this->passesOptionalCheck($attribute)
            && $this->isNotNullIfMarkedAsNullable($rule, $attribute);
    }

    /**
     * 确定该字段是否存在，或者规则意味着required
     *
     * @param  object|string  $rule
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    protected function presentOrRuleIsImplicit($rule, $attribute, $value)
    {
        if (is_string($value) && '' === trim($value)) {
            return $this->isImplicit($rule);
        }

        return $this->validatePresent($attribute, $value) || $this->isImplicit($rule);
    }

    /**
     * 确定一个给定的规则是否意味着属性是必需的。
     *
     * @param  object|string  $rule
     * @return bool
     */
    protected function isImplicit($rule): bool
    {
        return $rule instanceof ImplicitRule
            || in_array($rule, $this->implicitRules);
    }

    /**
     * 确定属性是否通过任何可选检查。
     *
     * @param  string  $attribute
     * @return bool
     */
    protected function passesOptionalCheck($attribute): bool
    {
        if (! $this->hasRule($attribute, ['Sometimes'])) {
            return true;
        }

        $data = ValidationData::initializeAndGatherData($attribute, $this->data);

        return array_key_exists($attribute, $data)
            || array_key_exists($attribute, $this->data);
    }

    /**
     * 判断属性是否未通过nullable检查。
     *
     * @param  string|object|Closure|Rule  $rule
     * @param  string  $attribute
     * @return bool
     */
    protected function isNotNullIfMarkedAsNullable($rule, string $attribute): bool
    {
        if ($this->isImplicit($rule) || ! $this->hasRule($attribute, ['Nullable'])) {
            return true;
        }

        return ! is_null(Arr::get($this->data, $attribute, 0));
    }

    /**
     * 抛出失败规则和错误信息。
     *
     * @param string $attribute
     * @param string $rule
     * @param array $parameters
     * @return void
     *
     * @throws ValidationException
     */
    public function throwFailure(string $attribute, string $rule, array $parameters = [])
    {
        $attribute = str_replace(
            [$this->dotPlaceholder, '__asterisk__'],
            ['.', '*'],
            $attribute
        );

        throw (new ValidationException($this->makeReplacements(
            $this->getMessage($attribute, $rule),
            $attribute,
            $rule,
            $parameters
        )))->setAttribute($attribute);
    }

    /**
     * 用给定的键替换每个带星号的字段参数。
     *
     * @param  array  $parameters
     * @param  array  $keys
     * @return array
     */
    protected function replaceAsterisksInParameters(array $parameters, array $keys): array
    {
        return array_map(function ($field) use ($keys) {
            return vsprintf(str_replace('*', '%s', $field), $keys);
        }, $parameters);
    }

    /**
     * 确定给定规则是否依赖于其他字段。
     *
     * @param  string|object|Rule|Closure  $rule
     * @return bool
     */
    protected function dependsOnOtherFields($rule): bool
    {
        return in_array($rule, $this->dependentRules);
    }

    /**
     * 从一个用点符号扁平化的属性中获取显式键。
     *
     * E.g. 'foo.1.bar.spark.baz' -> [1, 'spark'] for 'foo.*.bar.*.baz'
     *
     * @param  string  $attribute
     * @return array
     */
    protected function getExplicitKeys(string $attribute): array
    {
        $pattern = str_replace('\*', '([^\.]+)', preg_quote($this->getPrimaryAttribute($attribute), '/'));

        if (preg_match('/^' . $pattern . '/', $attribute, $keys)) {
            array_shift($keys);

            return $keys;
        }

        return [];
    }

    /**
     * 获取主属性名称。
     *
     * 例如，如果给出 "name.0"，将返回 "name.*"。
     *
     * @param string $attribute
     * @return string
     */
    protected function getPrimaryAttribute(string $attribute): string
    {
        foreach ($this->implicitAttributes as $unparsed => $parsed) {
            if (in_array($attribute, $parsed, true)) {
                return $unparsed;
            }
        }

        return $attribute;
    }

    /**
     * 获取给定属性的值。
     *
     * @param  string  $attribute
     * @return mixed
     */
    protected function getValue(string $attribute)
    {
        return Arr::get($this->data, $attribute);
    }

    /**
     * 获取给定属性的规则和参数。
     *
     * @param string $attribute
     * @param  string|array  $rules
     * @return array|null
     */
    protected function getRule(string $attribute, $rules): ?array
    {
        if (! array_key_exists($attribute, $this->rules)) {
            return null;
        }

        $rules = (array) $rules;

        foreach ($this->rules[$attribute] as $rule) {
            [$rule, $parameters] = ValidationRuleParser::parse($rule);

            if (in_array($rule, $rules)) {
                return [$rule, $parameters];
            }
        }

        return null;
    }

    /**
     * 确定给定属性在给定集合中是否有规则。
     *
     * @param string $attribute
     * @param  string|array  $rules
     * @return bool
     */
    public function hasRule(string $attribute, $rules): bool
    {
        return ! is_null($this->getRule($attribute, $rules));
    }
    
    /**
     * 获取验证中的数据。
     *
     * @return array
     */
    public function getData(): array
    {
        return $this->data;
    }

    /**
     * 获取验证规则。
     *
     * @return array
     */
    public function getRules()
    {
        return $this->rules;
    }
    
    /**
     * 获取存在验证器的实现。
     *
     * @param  string|null  $connection
     * @return PresenceVerifierInterface
     *
     * @throws \RuntimeException
     */
    public function getPresenceVerifier(?string $connection = null): PresenceVerifierInterface
    {
        if (! isset($this->presenceVerifier)) {
            throw new \RuntimeException('未提供存在验证器');
        }

        if ($this->presenceVerifier instanceof PresenceVerifierInterface) {
            $this->presenceVerifier->setConnection($connection);
        }

        return $this->presenceVerifier;
    }

    /**
     * 设置存在验证器的实现。
     *
     * @param  PresenceVerifierInterface  $presenceVerifier
     * @return void
     */
    public function setPresenceVerifier(PresenceVerifierInterface $presenceVerifier)
    {
        $this->presenceVerifier = $presenceVerifier;
    }
    
    /**
     * 注册一个自定义隐式验证器扩展数组。
     *
     * @param  array  $extensions
     * @return void
     */
    public function addImplicitExtensions(array $extensions)
    {
        $this->addExtensions($extensions);

        foreach ($extensions as $rule => $extension) {
            $this->implicitRules[] = Str::studly($rule);
        }
    }

    /**
     * 注册一个自定义依赖的验证器扩展数组。
     *
     * @param  array  $extensions
     * @return void
     */
    public function addDependentExtensions(array $extensions)
    {
        $this->addExtensions($extensions);

        foreach ($extensions as $rule => $extension) {
            $this->dependentRules[] = Str::studly($rule);
        }
    }

    /**
     * 注册一个自定义验证器信息替换器的数组。
     *
     * @param  array  $replacers
     * @return void
     */
    public function addReplacers(array $replacers)
    {
        if ($replacers) {
            $keys = array_map([Str::class, 'snake'], array_keys($replacers));

            $replacers = array_combine($keys, array_values($replacers));
        }

        $this->replacers = array_merge($this->replacers, $replacers);
    }

    /**
     * 注册一个自定义验证器扩展数组。
     *
     * @param  array  $extensions
     * @return void
     */
    public function addExtensions(array $extensions)
    {
        if ($extensions) {
            $keys = array_map([Str::class, 'snake'], array_keys($extensions));

            $extensions = array_combine($keys, array_values($extensions));
        }

        $this->extensions = array_merge($this->extensions, $extensions);
    }

    /**
     * 设置验证器的后备信息。
     *
     * @param  array  $messages
     * @return void
     */
    public function setFallbackMessages(array $messages)
    {
        $this->fallbackMessages = $messages;
    }

    /**
     * 调用一个基于类的验证器扩展。
     *
     * @param  string  $callback
     * @param  array  $parameters
     * @return bool
     */
    protected function callClassBasedExtension(string $callback, array $parameters): bool
    {
        [$class, $method] = Str::parseCallback($callback, 'validate');

        return (new $class)->{$method}(...array_values($parameters));
    }

    /**
     * 调用一个自定义验证器扩展。
     *
     * @param  string  $rule
     * @param  array  $parameters
     * @return bool|null
     */
    protected function callExtension(string $rule, array $parameters)
    {
        $callback = $this->extensions[$rule];

        if (is_callable($callback)) {
            return $callback(...array_values($parameters));
        } elseif (is_string($callback)) {
            return $this->callClassBasedExtension($callback, $parameters);
        }
    }

    /**
     * 处理对类方法的动态调用。
     *
     * @param  string  $method
     * @param  array  $parameters
     * @return mixed
     *
     * @throws \BadMethodCallException
     */
    public function __call($method, $parameters)
    {
        $rule = Str::snake(substr($method, 8));

        if (isset($this->extensions[$rule])) {
            return $this->callExtension($rule, $parameters);
        }

        throw new \BadMethodCallException(sprintf(
            'Method %s::%s does not exist.',
            static::class,
            $method
        ));
    }
}
