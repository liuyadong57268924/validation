<?php

namespace Itwmw\Validation\Support;

class ValidationException extends \Exception
{
    /** @var string */
    public $attribute;

    public function setAttribute(string $attribute): ValidationException
    {
        $this->attribute = $attribute;
        return $this;
    }

    public function getAttribute(): string
    {
        return $this->attribute;
    }
}
