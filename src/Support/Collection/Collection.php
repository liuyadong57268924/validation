<?php

namespace Itwmw\Validation\Support\Collection;

use Illuminate\Support\Arr;

class Collection extends \Illuminate\Support\Collection
{
    /**
     * Get an item from the collection by key.
     *
     * @param  mixed  $key
     * @param  mixed  $default
     * @return mixed
     */
    public function get($key, $default = null)
    {
        return data_get($this->items, $key, $default);
    }

    /**
     * Determine if an item exists in the collection by key.
     *
     * @param  mixed  $key
     * @return bool
     */
    public function has($key): bool
    {
        $keys = is_array($key) ? $key : func_get_args();

        foreach ($keys as $value) {
            if (!Arr::has($this->items, $value)) {
                return false;
            }
        }

        return true;
    }

    /**
     * Execute when the specified field exists
     *
     * @param mixed         $key         Fields to be validated
     * @param callable      $callback    Methods of execution
     * @param callable|null $default     Methods to execute when not present
     * @return \Illuminate\Support\HigherOrderWhenProxy|Collection|mixed
     * @noinspection PhpFullyQualifiedNameUsageInspection
     */
    public function whenHas($key, callable $callback, callable $default = null)
    {
        return $this->when($this->has($key), $callback, $default);
    }

    /**
     * Execute when the specified field does not exist
     *
     * @param mixed         $key           Fields to be validated
     * @param callable      $callback      Methods to execute when not present
     * @param callable|null $default
     * @return \Illuminate\Support\HigherOrderWhenProxy|Collection|mixed
     * @noinspection PhpFullyQualifiedNameUsageInspection
     */
    public function whenNotHas($key, callable $callback, callable $default = null)
    {
        return $this->when(!$this->has($key), $callback, $default);
    }

    /**
     * Write the specified value in the collection
     *
     * @param mixed $key
     * @param mixed $value
     * @return $this
     */
    public function set($key, $value)
    {
        Arr::set($this->items, $key, $value);
        return $this;
    }

    public function __get($key)
    {
        if (array_key_exists($key, $this->items)) {
            return $this->offsetGet($key);
        }

        return parent::__get($key);
    }

    public function __set($key, $value)
    {
        $this->offsetSet($key, $value);
    }
}
